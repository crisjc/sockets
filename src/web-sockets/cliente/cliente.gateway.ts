
import {
    WebSocketGateway,
    WebSocketServer,
    SubscribeMessage,
    WsResponse,
    OnGatewayInit,
    OnGatewayConnection,
    OnGatewayDisconnect
} from '@nestjs/websockets';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
const io = require('socket.io-client');
@WebSocketGateway(3002, { namespace: '/cliente' })
export class ClienteGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {


    usuarios: any[] = [];
    afterInit(server: any) {
        console.log('Init Cliente');
    }
    handleConnection(client: any, ...args: any[]) {
        console.log('cliente conectado', client.id, args);
    }
    handleDisconnect(client: any) {
        console.log('se desconecto el cliente: ', client.id);
    }


    @SubscribeMessage('saludarCliente')
    holaUsuario(client, data): Observable<WsResponse<number>> {
        console.log('Entro a cliente gateway', client.id);
        client.broadcast.emit('respuestaSaludoCliente', data)
        return data; // la peticion
    }

/*     @SubscribeMessage('solicitarLogin')
    solicitarLogin(client, nombreUsuario): Observable<WsResponse<number>> {
        let respuesta;
        this.usuarios.forEach(valor => {
            console.log('Entro el usuario al gateway', client.id);
            let condicionExisteUsuario = client.id === valor.id

            if (condicionExisteUsuario) {
                respuesta = {};
            } else {
                this.usuarios.push({ id: client.id, nombre: nombreUsuario })
                client.broadcast.emit('respuestaLogin', nombreUsuario)
                respuesta = nombreUsuario;
            }
            // la peticion
        })
        return respuesta;
    } */

    @SubscribeMessage('solicitarLogin')
	solicitarLogin(client, nombreUsuario): Observable<WsResponse<number>> {
        console.log('Entro el usuario',client.id);
        this.usuarios.push({id:client.id, nombre:nombreUsuario})
		client.broadcast.emit('respuestaLogin',nombreUsuario)
		return nombreUsuario; // la peticion
    }
    
    
    @SubscribeMessage('solicitarMensaje')
    solicitarMensaje(client, objetoMensaje): Observable<WsResponse<number>> {
        console.log('Entro el usuario', client.id, objetoMensaje);
        client.broadcast.emit('respuestaMensaje', objetoMensaje)
        return objetoMensaje; // la peticion
    }
}


// client.broadcast.emit('respuestaUsuario',data) // los sockets que escuchan 'events'