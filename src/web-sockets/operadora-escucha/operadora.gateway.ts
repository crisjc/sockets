import {
    WebSocketGateway,
    WebSocketServer,
    SubscribeMessage,
    WsResponse,
    OnGatewayInit,
    OnGatewayConnection,
    OnGatewayDisconnect
} from '@nestjs/websockets';
import { from, Observable, ObjectUnsubscribedError } from 'rxjs';
import { map } from 'rxjs/operators';
const io = require('socket.io-client');
@WebSocketGateway(3002,{namespace:'/operadora'})
export class OperadoraGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect{
    
    usuarios: any[] = [];
    rooms: any[] = [];
    afterInit(server: any) {
        console.log('Init Operadora-escucha');
        
    }
    handleConnection(client: any, ...args: any[]) {
        console.log('Operadora -> conexion de cliente', client.id, args);
    }
    handleDisconnect(client: any) {
        console.log('se desconecto el cliente: ', client.id);
    }

    @SubscribeMessage('solicitarLoginO')
	solicitarLoginO(client, nombreUsuario): Observable<WsResponse<number>> {
        console.log('Entro la operadora',client.id);
        this.usuarios.push({id:client.id, nombre:nombreUsuario})
		client.broadcast.emit('respuestaLogin',nombreUsuario)
		return nombreUsuario; // la peticion
    }
	@SubscribeMessage('saludarOperadora')
	holaUsuarioEnOperadora(client, data): Observable<WsResponse<number>> {
		console.log('Entro a operadora gateway',client.id);
		client.broadcast.emit('respuestaSaludoOperadora',data)
		return data; // la peticion
    }
    @SubscribeMessage('peticionUnirseRoom')
	peticionUnirseRoom(client,objectoRoom): Observable<WsResponse<number>> {
        console.log('Entro a operadora gateway',client.id);
        this.rooms.push(objectoRoom.room)
        client.join(objectoRoom.room)//  string
		client.broadcast.emit('respuestaUnionRoom',objectoRoom)
		return objectoRoom; // la peticion
    }

    @SubscribeMessage('enviarMensajeRoom')
	enviarMensaje(client,objectoRoom): Observable<WsResponse<number>> {
        console.log('Entro a enviar mensaje room',client.id,objectoRoom);
		client.to(objectoRoom.room).emit('respuestaMensajeRoom',objectoRoom)
		return objectoRoom; // la peticion
    }
    @SubscribeMessage('peticionSalirRoom')
	slairRoom(client,room): Observable<WsResponse<number>> {
        console.log('Entro a enviar mensaje room',client.id,room);

        client.leave(room)
        client.to(room).emit('respuestaSalioRoom',room)
        client.broadcast.emit('respuestasSalioRoomBroadcast',room)
		return room; // la peticion
    }



}