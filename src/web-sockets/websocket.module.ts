import { Module } from "@nestjs/common";
import { ClienteGateway } from "./cliente/cliente.gateway";
import { OperadoraGateway } from "./operadora-escucha/operadora.gateway";

@Module({
    imports:[ClienteGateway,OperadoraGateway],
    exports:[OperadoraGateway]
})

export class WebSocketModule{}